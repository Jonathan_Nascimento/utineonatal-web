from rest_framework.serializers import ModelSerializer

from AppTablet.models import Professional


class ProfessionalSerializer(ModelSerializer):
    class Meta:
        model = Professional
        fields = '__all__'

