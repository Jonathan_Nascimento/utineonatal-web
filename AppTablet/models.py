from django.db import models
from django.contrib.auth.models import AbstractUser


SETORES = ((0, 'Enfermagem'),
           (1, 'Obstetricia'))


class Professional(AbstractUser):
    nome_completo = models.CharField(max_length=50, null=True)
    cadastro_profissional = models.CharField(max_length=50, unique=True, null=True)
    setor = models.IntegerField(choices=SETORES, null=True)


    class Meta:
        verbose_name = 'Profissional'
        verbose_name_plural = 'Profissionais'

    def __unicode__(self):
        return u'{}'.format(self.nome_completo)
