from rest_framework.routers import DefaultRouter

from AppTablet.views import ProfessionaViewSet

router = DefaultRouter(trailing_slash=False)

router.register('user', ProfessionaViewSet)
urlpatterns = router.urls
