from django.apps import AppConfig


class ApptabletConfig(AppConfig):
    name = 'AppTablet'
    verbose_name = "Tablet"
