# Create your views here.
from rest_framework.renderers import JSONRenderer
from rest_framework.viewsets import ModelViewSet

from AppTablet.models import Professional
from AppTablet.serializers import ProfessionalSerializer


class CustomRender(JSONRenderer):
    def render(self, data, accepted_media_type=None, renderer_context=None):
        data = {'detail': data}
        return super(CustomRender, self).render(data, accepted_media_type, renderer_context)


class BaseViewSet(ModelViewSet):
    renderer_classes = (CustomRender,)
    pass


class ProfessionaViewSet(BaseViewSet):
    serializer_class = ProfessionalSerializer
    queryset = Professional.objects.all()
