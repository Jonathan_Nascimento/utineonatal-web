from django.contrib import admin

from AppTablet.models import Professional


class UserAdmin(admin.ModelAdmin):
    list_display = ('email', 'first_name', 'last_name', 'nome_completo', 'cadastro_profissional', 'setor')
    list_filter = ('is_staff', 'is_superuser')


admin.site.register(Professional, UserAdmin)
