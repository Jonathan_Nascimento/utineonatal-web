from django.apps import AppConfig


class AppmobileConfig(AppConfig):
    name = 'AppMobile'
    verbose_name = 'Mobile'
